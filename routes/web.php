<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/data-tables', 'IndexController@data_table');


Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/cast', 'CastController@index')->name('cast.index');
Route::get('/cast/create', 'CastController@create')->name('cast.create');
Route::post('/cast', 'CastController@store')->name('cast.store');
Route::get('/cast/{id}', 'CastController@show')->name('cast.show');
Route::get('/cast/{id}/edit', 'CastController@edit')->name('cast.edit');
Route::put('/cast/{id}', 'CastController@update')->name('cast.update');
Route::delete('/cast/{id}', 'CastController@destroy')->name('cast.destroy');
