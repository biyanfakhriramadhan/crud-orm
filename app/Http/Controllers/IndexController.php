<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $data = ['title' => "Index"];
        return view('home', $data);
    }

    public function data_table()
    {
        $data['title'] = "Data Table";
        return view('datatable', $data);
    }
}


